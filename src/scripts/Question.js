import $ from 'jquery';

export default class Question {
    constructor(question) {
        this.category = question.category;
        this.type = question.type;
        this.difficulty = question.difficulty;
        this.question = $('<textarea/>').html(question.question).text(); // decoding html encoded quotes and single quotes.
        this.correct_answer = $('<textarea/>').html(question.correct_answer).text(); // decoding html encoded quotes and single quotes.
        this.incorrect_answers = question.incorrect_answers.map(incorrect_answer => {
            return $('<textarea/>').html(incorrect_answer).text(); // decoding html encoded quotes and single quotes.
        });
    }

    /**
     * get property that shuffles and returns all possible answers
     * */
    get possibleAnswers() {
        if (this.type === 'boolean') {
            return ['True', 'False'];
        }
        let possibleAnswers = this.incorrect_answers.concat(this.correct_answer);
        for (let i = possibleAnswers.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            [possibleAnswers[i], possibleAnswers[j]] = [possibleAnswers[j], possibleAnswers[i]];
        }
        return possibleAnswers;
    }
}