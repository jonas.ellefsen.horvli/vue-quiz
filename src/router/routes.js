import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home';
import Play from '../views/Play';
import GameOverView from '../views/GameOverView';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/play',
    name: 'play',
    component: Play
  },
  {
    path: '/game-over',
    name: 'game-over',
    component: GameOverView
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
