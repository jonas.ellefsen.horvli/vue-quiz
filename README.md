# vue-assignment

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Components

### GameMenu
The main page of the app. User can enter name and start game from here.


### GameOver
Displays the results of the previous game if any was played, and lets user play again or quit to main page.

### GamePlay
Container for all the game logic. Fetches questions and displays them using the component QuizQuestion

#### QuizQuestion
Shows the question received as props from parent container GamePlay.
